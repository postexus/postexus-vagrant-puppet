class postexus_dev {
	package { [ 'vim', 'apache2',  'apache2-mpm-worker', 'apache2-suexec-custom', 'php5', 'php5-fpm',
		'php5-cli', 'php5-curl', 'php5-mysql', 'libapache2-mod-fastcgi',
		'libyaml-dev', 'php-pear', 'php5-dev', 'php5-xdebug' ]:
		ensure	=> present,
	}

	# Add repositories
	# MariaDB
	apt::source { "mariadb":
	  location          => "http://mirrors.supportex.net/mariadb/repo/5.5/ubuntu",
	  release           => "precise",
	  repos             => "main",
	  required_packages => "debian-keyring debian-archive-keyring",
	  key               => "1BB943DB",
	  key_server        => "keyserver.ubuntu.com",
	  include_src       => false
	}

	package { [ 'mariadb-server-5.5' ]:
		ensure	=> latest,
		require => Apt::Source["mariadb"],
	}

	# Apache
	file { "/etc/apache2/mods-available/fastcgi.conf":
	  owner   => root,
	  group   => root,
	  mode    => 0644,
	  ensure  => present,
	  replace => true,
	  source  => "puppet:///modules/postexus/apache/mods_available/fastcgi.conf",
	  notify  => Exec["restart-apache2"],
	  require => Package["libapache2-mod-fastcgi"],
	}

	file { '/etc/apache2/mods-enabled/fastcgi.conf':
	  ensure 	=> link,
	  target 	=> '/etc/apache2/mods-available/fastcgi.conf',
	  notify 	=> Exec["restart-apache2"],
	  require	=> [ Package["apache2"], File["/etc/apache2/mods-available/fastcgi.conf"] ]
	}

	file { "/etc/apache2/sites-available/000-default":
	  owner   => root,
	  group   => root,
	  mode    => 0644,
	  ensure  => present,
	  replace => true,
	  source  => "puppet:///modules/postexus/apache/000-default",
	  notify  => Exec["restart-apache2"],
	  require => Package["apache2"],
	}

	file { '/etc/apache2/sites-enabled/000-default':
	  ensure 	=> link,
	  target 	=> '/etc/apache2/sites-available/000-default',
	  notify 	=> Exec["restart-apache2"],
	  require	=> Package["apache2"],
	}

	exec { "/usr/sbin/a2enmod rewrite":
      notify 	=> Exec["reload-apache2"],
      require 	=> Package['apache2'],
    }

    exec { "/usr/sbin/a2enmod suexec":
      notify 	=> Exec["reload-apache2"],
      require 	=> Package['apache2'],
    }

    exec { "/usr/sbin/a2enmod actions":
      notify 	=> Exec["reload-apache2"],
      require 	=> Package['apache2'],
    }

    exec { "/usr/sbin/a2enmod alias":
      notify 	=> Exec["reload-apache2"],
      require 	=> Package['apache2'],
    }

    exec { "reload-apache2":
      command 		=> "/usr/bin/service apache2 reload",
      refreshonly 	=> true,
   	}

   	exec { "restart-apache2":
      command 		=> "/usr/bin/service apache2 restart",
      refreshonly 	=> true,
   	}

   	exec { "restart-php5-fpm":
      command 		=> "/usr/bin/service php5-fpm restart",
      refreshonly 	=> true,
   	}

   	file { '/var/www':
      ensure => link,
      owner   => www-data,
	  group   => www-data,
      target => "/postexus.git",
      notify => Exec["reload-apache2"],
      force  => true
    }

    file { "/etc/php5/fpm/pool.d/www.conf":
	  owner   => root,
	  group   => root,
	  mode    => 0644,
	  ensure  => present,
	  source  => "puppet:///modules/postexus/apache/www.conf",
	  notify  => Exec["restart-php5-fpm"],
	  require => Package["php5-fpm"],
	}
}